<?php

abstract class Delivery
{
    protected string $base_url;
    protected int $sourceKladr;
    protected int $targetKladr;
    protected int $weight;

    public function __construct($base_url, $sourceKladr, $targetKladr, $weight)
    {
        $this->base_url = $base_url;
        $this->sourceKladr = $sourceKladr;
        $this->targetKladr = $targetKladr;
        $this->weight = $weight;
    }

    abstract public function calcDelivery();
}

class FastDelivery extends Delivery
{
    /**
     * @throws Exception
     */
    public function calcDelivery(): array
    {
        $price = $this->priseByWeight($this->weight);

        $current_time = new DateTime('now', new DateTimeZone('Europe/Moscow'));
        $cutoff_time = new DateTime('18:00:00', new DateTimeZone('Europe/Moscow'));

        if ($current_time > $cutoff_time) {
            return [
                'price' => null,
                'date' => null,
                'error' => 'Requests after 18:00 are not accepted.'
            ];
        }

        $period = 2;
        $error = null;

        return [
            'price' => $price,
            'date' => date('Y-m-d', strtotime("+{$period} days")),
            'error' => $error
        ];
    }

    public function priseByWeight($weight): int
    {
        if ($weight < 2) {
            $price = 20;
        } elseif ($weight > 2 && $weight < 10) {
            $price = 40;
        } else {
            $price = 60;
        }

        return $price;
    }
}

class SlowDelivery extends Delivery
{
    public const BASE_PRISE = 150;

    public function calcDelivery(): array
    {
        $coefficient = self::BASE_PRISE * 1.5;
        $period = 5;
        $date = date('Y-m-d', strtotime("+{$period} days"));
        $error = null;

        return [
            'coefficient' => $coefficient,
            'date' => $date,
            'error' => $error
        ];
    }
}

class DeliveryCalculator
{
    private array $shipments;
    private array $companies;

    public function __construct($shipments, $companies)
    {
        $this->shipments = $shipments;
        $this->companies = $companies;
    }

    public function calculateDelivery(): array
    {
        $results = [];

        foreach ($this->shipments as $shipment) {
            $shipment_results = [];

            foreach ($this->companies as $company) {
                $delivery = new $company['class']($company['base_url'], $shipment['sourceKladr'], $shipment['targetKladr'], $shipment['weight']);
                $delivery_result = $delivery->calcDelivery();

                $shipment_results[] = [
                    'company' => $company['name'],
                    'price' => $delivery_result['coefficient'] ?? $delivery_result['price'] ?? null,
                    'date' => $delivery_result['date'],
                    'error' => $delivery_result['error']
                ];
            }

            if (count($shipment_results) > 0) {
                $results[] = [
                    'sourceKladr' => $shipment['sourceKladr'],
                    'targetKladr' => $shipment['targetKladr'],
                    'weight' => $shipment['weight'],
                    'results' => $shipment_results
                ];
            }
        }

        return $results;
    }
}

$shipments = [
    ['sourceKladr' => '1234567890', 'targetKladr' => '0987654321', 'weight' => 3],
    ['sourceKladr' => '0987654321', 'targetKladr' => '1234567890', 'weight' => 20]
];

$companies = [
    ['name' => 'Fast Delivery', 'class' => 'FastDelivery', 'base_url' => 'https://fastdelivery.com'],
    ['name' => 'Slow Delivery', 'class' => 'SlowDelivery', 'base_url' => 'https://slowdelivery.com']
];

$calculator = new DeliveryCalculator($shipments, $companies);
$results = $calculator->calculateDelivery();

$formatted_results = [];

foreach ($results as $result) {
    $formatted_shipment_results = [];

    foreach ($result['results'] as $shipment_result) {
        $formatted_shipment_results[] = [
            'price' => $shipment_result['price'],
            'date' => $shipment_result['date'],
            'error' => $shipment_result['error']
        ];
    }

    $formatted_results[] = [
        'sourceKladr' => $result['sourceKladr'],
        'targetKladr' => $result['targetKladr'],
        'weight' => $result['weight'],
        'results' => $formatted_shipment_results
    ];
}

print_r(json_encode($formatted_results, JSON_THROW_ON_ERROR));
